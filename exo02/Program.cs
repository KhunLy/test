﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez introduire 5 entiers");
            int[] tab = new int[5];
            int i=0;
            while (i < 5)
            {
                Console.WriteLine("Entier" + (i+1));
                int nombre;
                while (!int.TryParse(Console.ReadLine(), out nombre))
                    Console.WriteLine("Valeur incorrect");
                tab[i] = nombre;
                i++;
            }
            int total = 0;
            foreach (int item in tab)
            {
                total += item;
            }
            Console.WriteLine("la moyenne est : " + (total / tab.Length));
            Console.ReadKey();
        }
    }
}
