﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo00
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez introduire un nombre de seconde");
            string seconde = Console.ReadLine();
            int nbrSeconde = int.Parse(seconde);
            int jours = nbrSeconde / 86400;
            int heures = (nbrSeconde % 86400) / 3600;
            int minutes = (nbrSeconde % 3600) / 60;
            int secondes2 = nbrSeconde % 60;
            Console.WriteLine($"Il y a {jours} jours, {heures} heures, {minutes} minutes, {secondes2} secondes");
            Console.ReadKey();


        }
    }
}
