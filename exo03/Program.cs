﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo03
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> liste = new List<int> ();
            Console.WriteLine("Veuillez introduire des entiers");
            int entree;
            while (int.TryParse(Console.ReadLine(), out entree))
            {
                int nombre = entree;
                liste.Add(nombre);
            }
            int total = 0;
            foreach (int item in liste)
            {
                total += item;
            }
            Console.WriteLine("la moyenne est : " + (total / liste.Count));
            Console.ReadKey();
        }
    }
}
